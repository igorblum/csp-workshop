# Nonce bypasses

1. [https://cspws.ga/3-nonce-1.php?xss=1](https://cspws.ga/3-nonce-1.php?xss=1)

2. [Two ways](https://cspws.ga/3-nonce-2.php?xss=%3Ch1%3EHello!%3C/h1%3E) - FF

3. [https://cspws.ga/3-nonce-3.php?xss=%username%](https://cspws.ga/3-nonce-3.php?xss=%username%)

4. Bypass via CSS (FF)
	- Victim: [https://cspws.ga/3-nonce-4-CSS.php#%3Cs%3Etest%3C/s%3E](http://cspws.ga/3-nonce-4-CSS.php#%3Cs%3Etest%3C/s%3E)
	- Attacker: [https://3v1l.cf/3-4-exploit.php](https://3v1l.cf/3-4-exploit.php)

5. DOM XSS + Cache
	- Victim: [https://cspws.ga/3-nonce-5-dom.php#](https://cspws.ga/3-nonce-5-dom.php#)
	- Attacker: [https://3v1l.cf/3-5-attack.php](https://3v1l.cf/3-5-attack.php)

6. Social engineering (FF)
	- Victim: [https://cspws.ga/3-nonce-6-se.php?#](https://cspws.ga/3-nonce-6-se.php?#)
	- Attacker: [https://3v1l.cf/3-6-se.php](https://3v1l.cf/3-6-se.php)

7. XSLT (on the same origin) - Chrome:
	[https://cspws.ga/3-nonce-7-attlist.xml](https://cspws.ga/3-nonce-7-attlist.xml)