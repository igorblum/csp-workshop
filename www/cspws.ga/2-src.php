<?php

header('X-XSS-Protection: 0');

$csp_header = '';
$csp = (isset($_REQUEST['csp']) && $_REQUEST['csp'])?(string)$_REQUEST['csp']:'';

if($csp == 'wildcard') {
    $csp_header = "Content-Security-Policy: default-src *";
}
elseif($csp == 'none') {
    $csp_header = "Content-Security-Policy: default-src 'none'";
}
elseif($csp == 'data') {
    $csp_header = "Content-Security-Policy: default-src 'self' data:";
}
elseif($csp == 'sub') {
    $csp_header = "Content-Security-Policy: default-src *.cspws.ga";
}
elseif($csp == 'wl-script') {
    $csp_header = "Content-Security-Policy: default-src http://3v1l.cf/1.js";
}
elseif($csp == 'redirection') {
    $csp_header = "Content-Security-Policy: default-src 'self' http://3v1l.cf/1.js";
}
elseif($csp == 'unsafe-inline') {
    $csp_header = "Content-Security-Policy: script-src 'unsafe-inline'";
}
elseif($csp == 'unsafe-eval') {
    $csp_header = "Content-Security-Policy: script-src 'unsafe-eval' 'nonce-RANDOM'";
}
elseif($csp == 'nonce') {
    $csp_header = "Content-Security-Policy: script-src 'nonce-RANDOM'";
}
elseif($csp == 'sha256') {
    $csp_header = "Content-Security-Policy: script-src 'sha256-qznLcsROx4GACP2dm0UCKCzCG+HiZ1guq6ZZDob/Tng='";
}
elseif($csp == 'cdn') {
    $csp_header = "Content-Security-Policy: script-src https://googleads.g.doubleclick.net/";
}
elseif($csp == 'self') {
    $csp_header = "Content-Security-Policy: default-src 'self'";
}
elseif($csp == 'wo-strict-dynamic') {
    $csp_header = "Content-Security-Policy: script-src 'nonce-RANDOM'";
}
elseif($csp == 'strict-dynamic') {
    $csp_header = "Content-Security-Policy: script-src 'nonce-RANDOM' 'strict-dynamic'";
}

if($csp_header){
    header($csp_header);
}

?><!DOCTYPE html>
<html>
<head>
    <title>Wht is CSP</title>
</head>
<body>
    <a href="?csp=wildcard"> * </a> | <a href="?csp=none">'none'</a> | <a href="?csp=data">data:</a> | <a href="?csp=sub">subdomain</a> | <a href="?csp=wl-script">whitelisted script</a> | <a href="?csp=redirection">redirection + whitelisted</a> | <a href="?csp=unsafe-inline">Unsafe inline</a> | <a href="?csp=nonce">nonce (RANDOM)</a> | <a href="?csp=unsafe-eval">Unsafe eval</a> | <a href="?csp=sha256">sha256</a> | <a href="?csp=cdn">cdn</a> | <a href="?csp=self">Image</a> | <a href="?csp=wo-strict-dynamic">Without strict-dynamic</a> | <a href="?csp=strict-dynamic">strict-dynamic</a>
    <br/><br/>
    <b><?php if($csp_header){echo $csp_header;}?></b>
    <br/><br/>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>">
        <input type="hidden" id="csp" name="csp" value="<?=$csp;?>">
        <br/>
        <textarea name="xss" rows="20" cols="60"><?= htmlspecialchars(@$_REQUEST['xss']);?></textarea>
        <br/>
        <input type="submit" value="Submit">
    </form>
    <hr />
    <?php if(isset($_REQUEST['xss'])) {
        echo $_REQUEST['xss'];
    }
    ?>  
</body>
</html>

