<?php

function generateRandomString($length = 6) {
    $characters = '1234567890';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};

$nonce=generateRandomString(9);

header('X-XSS-Protection: 0');
header("Content-Security-Policy: script-src 'nonce-$nonce'; style-src 'unsafe-inline'; default-src *");

?>
<script nonce='<?=$nonce;?>'>document.write('URL ' + unescape(location.href))</script><script nonce='<?=$nonce;?>'>console.log('another nonced script')</script>
