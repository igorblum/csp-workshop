<?php

header('X-XSS-Protection: 0');

$csp_enbled = (isset($_REQUEST['csp_enbled']) && $_REQUEST['csp_enbled'])?1:0;
$csp = (isset($_REQUEST['csp']) && $_REQUEST['csp'])?(string)$_REQUEST['csp']:'';
$csp_header = '';
if($csp == 'default-src') {
    $csp_header = "Content-Security-Policy: default-src 'self'";
}
elseif($csp == 'script-src') {
    $csp_header = "Content-Security-Policy: script-src 'self'";
}
elseif($csp == 'style-src') {
    $csp_header = "Content-Security-Policy: style-src 'self'";
}
elseif($csp == 'img-src') {
    $csp_header = "Content-Security-Policy: img-src 'self'";
}
elseif($csp == 'connect-src') {
    $csp_header = "Content-Security-Policy: connect-src 'self' 'unsafe-inline'";
}
elseif($csp == 'font-src') {
    $csp_header = "Content-Security-Policy: font-src 'self'";
}
elseif($csp == 'child-src') {
    $csp_header = "Content-Security-Policy: child-src 'self'";
}
elseif($csp == 'form-action') {
    $csp_header = "Content-Security-Policy: form-action 'self'";
}
elseif($csp == 'media-src') {
    $csp_header = "Content-Security-Policy: media-src 'self'";
}
elseif($csp == 'base-uri') {
    $csp_header = "Content-Security-Policy: base-uri 'self'";
}
elseif($csp == 'report-uri') {
    $csp_header = "Content-Security-Policy: default-src 'self'; report-uri https://cspws.ga/csp-report.php";
}

if($csp_enbled && $csp_header){
    header($csp_header);
}
//phpinfo();

?><!DOCTYPE html>
<html>
<head>
    <title>Wht is CSP</title>
</head>
<body>
    <a href="?csp=script-src">script-src</a> | <a href="?csp=style-src">style-src</a> | <a href="?csp=img-src">img-src</a> | <a href="?csp=connect-src">connect-src</a> | <a href="?csp=font-src">font-src</a> | <a href="?csp=media-src">media-src</a> | <a href="?csp=child-src">child-src (frame-src)</a> | <a href="?csp=form-action">form-action</a> | <a href="?csp=frame-ancestors">frame-ancestors</a> | <a href="?csp=base-uri">base-uri</a> | <a href="?csp=report-uri">report-uri</a>
    <br/><br/>
    <b><?php if($csp_header){echo $csp_header;}?></b>
    <br/><br/>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>">
        <label for="csp_enbled">CSP enabled?</label><input type="checkbox" id="csp_enbled" name="csp_enbled" value="1" <?=$csp_enbled?'checked':'';?>/>
        <input type="hidden" id="csp" name="csp" value="<?=$csp;?>">
        <br/>
        <textarea name="xss" rows="20" cols="60"><?= htmlspecialchars(@$_REQUEST['xss']);?></textarea>
        <br/>
        <input type="submit" value="Submit">
    </form>
    <hr />
    <?php if(isset($_REQUEST['xss'])) {
        echo $_REQUEST['xss'];
    }
    ?>    <span id=secret>s0m3th1ng s3cr3t</span><div id="x" name=')y'> to read</div><a></a>
</body>
</html>

