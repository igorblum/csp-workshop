<!DOCTYPE html>
<html>
<head>
  <meta charset="utf8">
  <title>Fake XSS w/jQuery templates!</title>
  <meta http-equiv=content-security-policy content="object-src 'none';script-src 'nonce-secret' 'unsafe-eval';">
  <script nonce=secret type='text/javascript' src='http://code.jquery.com/jquery-1.8.3.js'></script>
  <script nonce=secret type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/emblem/0.4.0/emblem.min.js"></script>
  <script nonce=secret type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/ember.js/2.0.0/ember.min.js"></script>
  <script nonce=secret type='text/javascript'> 
    $(window).load(function(){
        Ember.Application.create();
    });
  </script>
</head>
<body>
  XSS XSS XSS <script type="text/x-emblem">script alert(1);</script> XSS XSS XSS
  Shamelessly stolen from <a href="https://github.com/cure53/mustache-security/blob/master/wiki/jQuery.wiki">mustache-security</a> by cure53.
</body>
</html>
