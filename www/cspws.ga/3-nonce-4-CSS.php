<?php

function generateRandomString($length = 6) {
    $characters = 'abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};

$nonce=generateRandomString(6);

header('X-XSS-Protection: 0');
header("Content-Security-Policy: default-src 'none';script-src 'nonce-$nonce' 'strict-dynamic'; style-src 'unsafe-inline' *; img-src *;");

?>
<body>
<div id=victim></div>
<script nonce=<?=$nonce;?>>
document.getElementById('victim').innerHTML=unescape(location.href);
</script>
</body>