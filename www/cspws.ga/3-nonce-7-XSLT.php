<html>
  <head>
    <title>CSP nonce</title>
    <meta http-equiv="content-security-policy" content="object-src 'none'; script-src 'nonce-secret';"/>
  </head>
  <body>
    <script nonce="secret">var a=1;</script>
  </body>
</html>