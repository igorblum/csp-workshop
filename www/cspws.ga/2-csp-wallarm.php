<?php
$csp_header = "Content-Security-Policy: default-src 'self' 'unsafe-inline'; sandbox allow-forms allow-same-origin allow-scripts allow-modals";
header($csp_header);
?><html>
<head></head>
<body>
<div class="center-block">
<b><?php if($csp_header){echo $csp_header;}?></b><br/><br/><br/>
<?php
function generateRandomString($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};

if(isset($_GET['xss'])){
echo "<script>".$_GET['xss']."</script>";
}
else {
header('Location: ?xss=alert("Welcome")');
}
echo "\n<code id='secret'>Secret: ".generateRandomString()."</code>"
?>
</div>
</body>
</html>
