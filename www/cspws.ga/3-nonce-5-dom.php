<?php

function generateRandomString($length = 6) {
    $characters = 'abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
};

$nonce=generateRandomString(6);

header('X-XSS-Protection: 0');
header("Content-Security-Policy: script-src 'nonce-$nonce'; default-src 'none'");
header('Cache-Control: max-age=99999999');

?>

<script nonce='<?=$nonce;?>'>document.write('URL ' + unescape(location.href))</script><script nonce='<?=$nonce;?>'>console.log('another nonced script')</script>
